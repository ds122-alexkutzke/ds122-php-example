<?php
  function cria_item_lista($item){
    return("<li>$item</li>");
  }
  function cria_lista($itens){
    $result = "<ul>";
    foreach ($itens as $key => $value) {
      $result .= cria_item_lista($value);
    }
    $result .= "</ul>";
    return($result);
  }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Testes de PHP</title>
</head>
<body>
  <?php
    $itens = array(1, 2, 4, 5, 6);

    echo cria_lista($itens);
  ?>
</body>
</html>
